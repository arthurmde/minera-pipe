class ResearchArea < ActiveRecord::Base
	has_many :project_research_area_relations
	has_many :projects, through: :project_research_area_relations
end
