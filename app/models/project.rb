class Project < ActiveRecord::Base
	belongs_to :responsible_researcher, class_name: 'Researcher'
	belongs_to :superviser, class_name: 'Researcher'
	belongs_to :visiting_researcher, class_name: 'Researcher'
	belongs_to :foreign_responsable_researcher, class_name: 'Researcher'

	has_many :project_research_area_relations
	has_many :research_areas, through: :project_research_area_relations

	has_many :project_researcher_relations
	has_many :researchers, through: :project_researcher_relations

	def valid_date?
		return :start_date < :end_date
	end

	validates :process,
						:allow_blank => false,
						:format => { :with => %r{\d{2}\/\d*\-\d}, :message => 'Este numero de processo não é valido' }

	validates :end_date,
						:presence => { :message => 'É um dado necessário' }

	validates :start_date,
						:presence => { :message => 'É um dado necessário' },
						:if => :valid_date?

	validates :title,
						:length => { :minimum => 5, :message => 'O título deve ter, no mínimo, 5 caracteres' }

	def self.abstracts
		hash = {}
		array = Project.pluck('process, abstract')
 		array.each do |pair|
			hash[pair.first] = pair.second
		end
		hash
	end
end
