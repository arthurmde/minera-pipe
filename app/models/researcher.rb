class Researcher < ActiveRecord::Base
	has_many :supervised_projects, class_name: 'Project', foreign_key: 'superviser_id'
	has_many :on_responsability_projects, class_name: 'Project', foreign_key: 'responsible_researcher_id'	
	has_many :visiting_projects, class_name: 'Project', foreign_key: 'visiting_researcher_id'
	has_many :foreign_projects, class_name: 'Project', foreign_key: 'foreign_responsable_researcher_id'

	has_many :project_researcher_relations
	has_many :projects, through: :project_researcher_relations
end
