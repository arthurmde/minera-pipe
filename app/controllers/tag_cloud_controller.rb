class TagCloudController < ApplicationController
  require 'cluster/headlines'
    def index
        @index = 0
        @size = 400
        @tag_array = []

        c = Clusterizer.new
        c.clusters.each do |tag|
            array = []
            tag.each do |key, value|
                array << {:text => key, :weight => value * 20}
            end
            @tag_array << array
        end
    end
end
