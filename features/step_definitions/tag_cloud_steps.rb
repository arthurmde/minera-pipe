require 'uri'
require 'cgi'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

module WithinHelpers
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
end
World(WithinHelpers)

When /^I open the page$/ do
  visit path_to("the home page")
end

Then /^The title should be "(.+)"$/ do |title|
  assert page.has_content?(title)
end