
class Clusterizer
  require 'rubygems'
  require 'bundler/setup'
  #require_relative 'kmeans-clusterer'
  #require_relative 'bag'
  require 'cluster/kmeans-clusterer'
  require 'cluster/bag'
  require 'optparse'
  require 'rake_text'

  def clusters
    k = 6
    runs = 20

    OptionParser.new do |opts|
      opts.on("-kK") {|v| k = v.to_i }
      opts.on("-rD") {|v| runs = v.to_i }
    end.parse!

    docs = []
    doc_ids = []
    wfreq = []
    wnote = []

    bag = BagOfWords.new idf: true
    filename = "lib/cluster/ignoredWords.txt"
    ignoredWords = File.open(filename).read.split(/\n/)
    Project.abstracts.each do |key, value|
      unless value.nil?
        value.downcase!
        words = value.split(/ |, |,|\.|"|;|:|\(|\)|\/|\[|\]|\{|\}/)
        relev = ""
        words.each do |i|
          if i.size > 3
            if not ignoredWords.include? i
              relev << " #{i}"
            end
          end
        end
        if relev.size > 500
          bag.add_doc relev
          docs << relev
          doc_ids << key
        end
      end
    end
    puts "\nClassifying #{docs.length} docs with #{bag.terms_count} unique terms into #{k} clusters:\n"
    data = bag.to_matrix

    start = Time.now
    kmeans = KMeansClusterer.run(k, data, runs: runs, log: true)
    elapsed = Time.now - start

    kmeans.clusters.each do |cluster|
      acc = Hash.new {|h, k| h[k] = []}
      grouped_points = cluster.points.inject(acc){|hsh, p| hsh[doc_ids[p.id]] << p; hsh }
      sums = grouped_points.map {|file, points| [file, points.length]}

      samplesize = 5
      samplesize = 2 if grouped_points.keys.length > 2
      samplesize = 1 if grouped_points.keys.length > 10

      frequency = Hash.new 0
      note  = Hash.new 0.0
      most = 0

      grouped_points.each do |name, points|
        points.sample(samplesize).each do |point|
          words = docs[point.id].split(/ /)
          relev = []
          words.each do |i|
            if i.size > 3
              relev << i
            end
          end
          relev.each do |word|
            frequency[word] += 1
          end
        end
      end
      freqAr = frequency.sort_by {|_key, value| value}
      puts freqAr[0][0]
      puts freqAr[0][1]
      wfreq[cluster.id] = frequency
      wfreq[cluster.id].each do |key, value|
        if value > most
          most = value
        end
      end
      ind = freqAr.size-20
      puts ind
      while ind < freqAr.size
        puts ind
        note[freqAr[ind][0]] = freqAr[ind][1].to_f/most*20
        ind += 1
      end
      wnote[cluster.id] = note
    end
    puts wnote
    return wnote
  end
end
