class CreateResearchers < ActiveRecord::Migration
  def change
    create_table :researchers do |t|
			t.string :name			
			t.string :institution, default: ''
      t.timestamps null: false
    end
  end
end
