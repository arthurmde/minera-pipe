class CreateProjectResearcherRelations < ActiveRecord::Migration
  def change
    create_table :project_researcher_relations do |t|
			t.belongs_to :project, index: true
			t.belongs_to :researcher, index: true
    end
  end
end
