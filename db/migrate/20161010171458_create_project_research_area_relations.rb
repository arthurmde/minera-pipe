class CreateProjectResearchAreaRelations < ActiveRecord::Migration
  def change
    create_table :project_research_area_relations do |t|
			t.belongs_to :project, index: true
			t.belongs_to :research_area, index: true
    end
  end
end
